#include "qemu/osdep.h"
#include "hw/boards.h"
#include "hw/arm/omap.h"
#include "exec/address-spaces.h"
#include "qapi/error.h"
#include "cpu.h"

#define BEAGLE_SDRAM_SIZE    (256 * 1024 * 1024) /* 256MB */
#define SDRAM_BASE_ADDRESS    0x01000000

typedef struct {
    MachineClass parent;
    //MuscaType type;
    //uint32_t init_svtor;
    int sram_addr_width;
    int sdram_addr_width;
    int num_irqs;
    //const MPCInfo *mpc_info;
    //int num_mpcs;
} BeagleMachineClass;

typedef struct {
    MachineState parent;

    //ARMSSE sse;
    ///* RAM and flash */
    MemoryRegion ram;
    //SplitIRQ cpu_irq_splitter[MUSCA_NUMIRQ_MAX];
    //SplitIRQ sec_resp_splitter;
    //TZPPC ppc[MUSCA_PPC_MAX];
    //MemoryRegion container;
    //UnimplementedDeviceState eflash[2];
    //UnimplementedDeviceState qspi;
    //TZMPC mpc[MUSCA_MPC_MAX];
    //UnimplementedDeviceState mhu[2];
    //UnimplementedDeviceState pwm[3];
    //UnimplementedDeviceState i2s;
    //PL011State uart[2];
    //UnimplementedDeviceState i2c[2];
    //UnimplementedDeviceState spi;
    //UnimplementedDeviceState scc;
    //UnimplementedDeviceState timer;
    //PL031State rtc;
    //UnimplementedDeviceState pvt;
    //UnimplementedDeviceState sdio;
    //UnimplementedDeviceState gpio;
    //UnimplementedDeviceState cryptoisland;
} BeagleMachineState;


#define TYPE_BEAGLE_MACHINE "beagle"

#define BEAGLE_MACHINE(obj) \
    OBJECT_CHECK(BeagleMachineState, obj, TYPE_BEAGLE_MACHINE)
#define BEAGLE_MACHINE_GET_CLASS(obj) \
    OBJECT_GET_CLASS(BeagleMachineClass, obj, TYPE_BEAGLE_MACHINE)
#define BEAGLE_MACHINE_CLASS(klass) \
    OBJECT_CLASS_CHECK(BeagleMachineClass, klass, TYPE_BEAGLE_MACHINE)

static void beagle_init(MachineState *machine)
{
    BeagleMachineState *bms = BEAGLE_MACHINE(machine);
    BeagleMachineClass *bmc = BEAGLE_MACHINE_GET_CLASS(bms);
    MachineClass *mc = MACHINE_GET_CLASS(machine);
    MemoryRegion *system_memory = get_system_memory();
    DriveInfo *dmtd = drive_get_next(IF_MTD);
    DriveInfo *dsd  = drive_get_next(IF_SD);
    MemoryRegion *sdram = g_new(MemoryRegion, 1);

    if (strcmp(machine->cpu_type, mc->default_cpu_type) != 0) {
        error_report("This board can only be used with CPU %s",
                     mc->default_cpu_type);
        exit(1);
    }
    memory_region_init_ram(sdram, NULL, "sdram", BEAGLE_SDRAM_SIZE,
                           &error_fatal);
    memory_region_add_subregion(system_memory, SDRAM_BASE_ADDRESS, sdram);

}

static void beagle_class_init(ObjectClass *oc, void *data)
{
    MachineClass *mc = MACHINE_CLASS(oc);

    mc->default_cpus = OMAP3_NCPUS;
    mc->min_cpus = mc->default_cpus;
    mc->max_cpus = mc->default_cpus;
    mc->default_cpu_type = ARM_CPU_TYPE_NAME("cortex-a8");
    mc->init = beagle_init;
}


static const TypeInfo beagle_info = {
    .name = TYPE_BEAGLE_MACHINE,
    .parent = TYPE_MACHINE,
    .abstract = true,
    .instance_size = sizeof(BeagleMachineState),
    .class_size = sizeof(BeagleMachineClass),
    .class_init = beagle_class_init,
};

static void beagle_machine_init(void)
{
    type_register_static(&beagle_info);
}

type_init(beagle_machine_init);
