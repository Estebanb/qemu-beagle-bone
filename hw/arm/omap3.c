#include "qemu/osdep.h"
#include "hw/boards.h"
#include "cpu.h"

static void omap3_machine_class_init(ObjectClass *oc, void *data)
{
    //DeviceClass *dc = DEVICE_CLASS(oc);
    //dc->realize = bcm2836_realize;
    //dc->props = bcm2836_props;
}

static const TypeInfo omap3_machine_type = {
    .name = MACHINE_TYPE_NAME("omap3"),
    .parent = TYPE_MACHINE,
    .class_init = omap3_machine_class_init,
};

static void omap3_machine_init(void)
{
    type_register_static(&omap3_machine_type);
}
type_init(omap3_machine_init)
